
def encrypt(to_encrypt, shift):
	to_encrypt = to_encrypt.upper().replace(" ", "").replace("\n", "").replace("\t", "")
	length = len(to_encrypt)

	# tworzymy dwuwymiarowa tablice i wypelniamy nullami
	fence = [[ None for x in range (length)] for y in range(shift)]

	# wprowadzamy litery w odpowiednie miejsca tablicy co kazda iteracje zmieniajac wiersz
	current_level = 0
	up = 1 # kierunek zmiany - dla 1 do dolu, dla -1 do gory
	for i in range(0, length):
		fence[current_level][i] = to_encrypt[i]
		
		current_level = current_level + (1 * up)
		
		if current_level == (shift - 1) or current_level == 0:
			up = up * (-1)

	# odczytujemy z tablicy zaszyfrowany string
	encrypted = ""
	for j in range(0, shift):
		for i in range(0, length):
			if fence[j][i] is not None:
				encrypted = encrypted + fence[j][i]

	return encrypted

#--------------------------------------------------------------------
def decrypt(to_decrypt, shift):
	length = len(to_decrypt)
	# tworzymy tablice
	fence = [[ None for x in range (length)] for y in range(shift)]

	# w miejsca gdzie beda litery wstawiamy gwiazdki
	current_level = 0
	up = 1
	for i in range(0, length):
		fence[current_level][i] = "*"
		
		current_level = current_level + (1 * up)
		
		if current_level == (shift - 1) or current_level == 0:
			up = up * (-1)
	
	# idac kolejnymi wierszami wypelniamy miejsca z gwiazdkami literami zaszyfrowanego stringa
	current_letter = 0
	for j in range(shift):
		for i in range(0, length):
			if fence[j][i] is not None:
				fence[j][i] = to_decrypt[current_letter]
				current_letter += 1
	
	# odszyfrowujemy string odczytujac litery z kolejnych kolumn
	decrypted = ""
	for i in range(0, length):
		for j in range(shift):
			if fence[j][i] is not None:
				decrypted += fence[j][i]
	
	return decrypted

#--------------------------------------------------------------------

option = raw_input("Enter 1 to encrypt, 2 to decrypt\n").strip()

if option != "1" and option != "2":
    print("Invalid option.")
    quit()

phrase = raw_input("Enter phrase.\n").strip()
steps = int(raw_input("Enter number of steps.\n"))

if option == "1":
	encrypted = encrypt(phrase, steps)
	print(encrypted)
elif option == "2":
	decrypted = decrypt(phrase, steps)
	print(decrypted)