# ASCII A-Z to 65-90
# chr(x) zmienia int x na odpowiadajacy char
# ord(x) zmienia char na odpowiadajacy mu int

import re

# zamien dowolna liczbe naturalna na char: 0 = "A", 1 = "B" itd.
total_chars = 91 - 65
def get_char(n):
    return chr(n % total_chars + 65)
# zmien dowolny char na liczbe naturalna: "A" = 0, "B" = 1 itd.
def get_num(ch):
    return ord(ch) - 65


def core_algorithm(phrase, key, operation):
    # zmieniamy na duze litery 
    phrase = phrase.upper().replace(" ", "").replace(" ", "")
    key = key.upper()

    # wyliczamy dlugosc frazy
    length = len(phrase)

    # sprawdzamy poprawnosc frazy i klucza
    if not re.match("^[A-Z]+$", phrase) or not re.match("^[A-Z]+$", key):
        raise ValueError("Only alphabetical characters allowed for phrase and key")
    if length != len(key):
        raise ValueError("Key must have the same length as phrase")
    
    # transformujemy kolejne chary w frazie i dodajemy do stringa result
    result = ""
    for i in range(length):
        target_num = operation(get_num(phrase[i]), get_num(key[i]))
        result += get_char(target_num)
    
    return result

# szyfrowanie przez wstrzykniecie lambdy dodajacej dwie wartosci
def encrypt(phrase, key):
    return core_algorithm(phrase, key, lambda x, y: x + y)

# deszyfrowanie przez wstrzykniecie lambdy odejmujacej dwie wartosci
def decrypt(phrase, key):
    return core_algorithm(phrase, key, lambda x, y: x - y)

#------------------------------------------------------------------
option = raw_input("Enter 1 for encryption or 2 for decryption:\n").strip()
if option != "1" and option != "2":
    print("Invalid option.")
    quit()

phrase = raw_input("Enter phrase:\n").strip()
key = raw_input("Enter key:\n").strip()

if option == "1":
    print("Result:\n" + encrypt(phrase, key))
elif option == "2":
    print("Result:\n" + decrypt(phrase, key))