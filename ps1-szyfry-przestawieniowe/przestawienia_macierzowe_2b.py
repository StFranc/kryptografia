from math import ceil

def encrypt(phrase, key):
    # usuwamy znaki biale
    phrase = phrase.upper().replace(" ", "").replace("\n", "").replace("\t", "")
    # dlugosc klucza
    key_len = len(key)
    #dlugosc szyfrowanej frazy
    phrase_len = len(phrase)
    # ilosc wierszy w tablicy
    rows_num = int(ceil(float(phrase_len)/key_len))

    # Tworzymy tabice 2-wymiarowa wypelniajac kolejne wiersze kolejnymi literami podanej frazy
    # x - wiersze, y - kolumny
    matrix = [[(phrase[x*key_len + y] if x*key_len + y < phrase_len else None) for y in range(0, key_len)] for x in range(0,rows_num)]

    # wyciagamy wagi kolejnych znakow z klucza i tworzymy tablice par (oryginalny_index, waga)
    weights = map(lambda el: (el[0],ord(el[1])), enumerate(key))
    # sortujemy otrzymana tablice po wagach
    sorted_weights = sorted(weights, key = lambda a: a[1])
    
    encrypted = ""
    # iterujemy po kolumnach w posortowanej kolejnosci i dodajemy kolejne znaki do zaszyfrowanwego stringa
    for (i, w) in sorted_weights:
        for j in range(0, rows_num):
            if matrix[j][i] is not None:
                encrypted += matrix[j][i]

    return encrypted
# ------------------------------------------------------------------------------------

def decrypt(phrase, key):
    # usuwamy znaki biale
    phrase = phrase.upper().replace(" ", "").replace("\n", "").replace("\t", "")
    # dlugosc klucza
    key_len = len(key)
    #dlugosc szyfrowanej frazy
    phrase_len = len(phrase)
    # ilosc wierszy w tablicy
    rows_num = int(ceil(float(phrase_len)/key_len))

    # Tworzymy tabice 2-wymiarowa i wypelniamy gwiazdkami pola w ktorych znajda sie znaki
    # x - wiersze, y - kolumny
    matrix = [["*" if x*key_len + y < phrase_len else "" for y in range(0, key_len)] for x in range(0,rows_num)]

    # wyciagamy wagi kolejnych znakow z klucza i tworzymy tablice par (oryginalny_index, waga)
    weights = map(lambda el: (el[0],ord(el[1])), enumerate(key))
    # sortujemy otrzymana tablice po wagach malejaco
    sorted_weights = sorted(weights, key = lambda a: a[1])
    
    # iterujemy po kolumnach w posortowanej kolejnosci i wpisujemy kolejne znaki zaszyfrowanej frazy do pol tablicy
    temp = phrase_len - 1
    for (i, w) in reversed(sorted_weights):
        for j in reversed(range(rows_num)):
            if matrix[j][i] is not "":
                matrix[j][i] = phrase[temp]
                temp -= 1

    # laczymy zawartosc tablicy w striga
    decrypted = "".join(map(lambda el: "".join(el), matrix))
    return decrypted

#----------------------------------------------------------
option = raw_input("Enter 1 for encryption or 2 for decryption:\n").strip()
if option != "1" and option != "2":
    print("Invalid option.")
    quit()

phrase = raw_input("Enter phrase:\n").strip()
key = raw_input("Enter key:\n").strip()

if option == "1":
    print("Result:\n" + encrypt(phrase, key))
elif option == "2":
    print("Result:\n" + decrypt(phrase, key))