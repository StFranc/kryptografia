from math import ceil

def encrypt(phrase, key):
    # usuwamy znaki biale
    phrase = phrase.replace(" ", "").replace("\n", "").replace("\t", "")
    # dlugosc klucza
    key_len = len(key)
    #dlugosc szyfrowanej frazy
    phrase_len = len(phrase)

    # wyciagamy wagi kolejnych znakow z klucza i tworzymy tablice par (oryginalny_index, waga)
    weights = map(lambda el: (el[0],ord(el[1])), enumerate(key))
    # sortujemy otrzymana tablice po wagach
    sorted_weights = map(lambda (i, el): (el[0], i), enumerate(sorted(weights, key = lambda a: a[1])))
    weights = sorted(sorted_weights, key = lambda a: a[0])

    # tworzymy i wypelniamy tablice
    matrix = [[None for x in range(key_len)]]
    curr_column = 0
    for i in range(phrase_len):
        # jesli nie osiagnelismy konca wiersza i obecna kolumna mniejsza od indexu klucza po sortowaniu - dodaj litere do ostatniego wiersza
        if curr_column >= key_len or curr_column > sorted_weights[len(matrix)-1][0]:
            matrix.append([None for x in range(key_len)])
            curr_column = 0
        matrix[len(matrix)-1][curr_column] = phrase[i]
        curr_column += 1

    print(weights)
    print(matrix)
    # ilosc wierszy w tablicy
    rows_num = len(matrix)

    encrypted = ""
    # iterujemy po kolumnach w posortowanej kolejnosci i dodajemy kolejne znaki do zaszyfrowanwego stringa
    for (i, w) in sorted_weights:
        for j in range(0, rows_num):
            if matrix[j][i] is not None:
                encrypted += matrix[j][i]

    return encrypted
# ------------------------------------------------------------------------------------

def decrypt(phrase, key):
    # usuwamy znaki biale
    phrase = phrase.replace(" ", "").replace("\n", "").replace("\t", "")
    # dlugosc klucza
    key_len = len(key)
    #dlugosc szyfrowanej frazy
    phrase_len = len(phrase)

    # wyciagamy wagi kolejnych znakow z klucza i tworzymy tablice par (oryginalny_index, waga)
    weights = map(lambda el: (el[0],ord(el[1])), enumerate(key))
    # sortujemy otrzymana tablice po wagach
    sorted_weights = map(lambda (i, el): (el[0], i), enumerate(sorted(weights, key = lambda a: a[1])))
    weights = sorted(sorted_weights, key = lambda a: a[0])
    
    # tworzymy i wypelniamy tablice
    matrix = [["" for x in range(key_len)]]
    curr_column = 0
    for i in range(phrase_len):
        # jesli nie osiagnelismy konca wiersza i obecna kolumna mniejsza od indexu klucza po sortowaniu - dodaj litere do ostatniego wiersza
        if curr_column >= key_len or curr_column > sorted_weights[len(matrix)-1][0]:
            matrix.append(["" for x in range(key_len)])
            curr_column = 0
        matrix[len(matrix)-1][curr_column] = "*"
        curr_column += 1

    # ilosc wierszy w tablicy
    rows_num = len(matrix)

    # iterujemy po kolumnach w posortowanej kolejnosci i wpisujemy kolejne znaki zaszyfrowanej frazy do pol tablicy
    temp = phrase_len - 1
    for (i, w) in reversed(sorted_weights):
        for j in reversed(range(rows_num)):
            if matrix[j][i] is not "":
                matrix[j][i] = phrase[temp]
                temp -= 1

    # laczymy zawartosc tablicy w striga
    decrypted = "".join(map(lambda el: "".join(el), matrix))
    return decrypted

#----------------------------------------------------------

option = raw_input("Enter 1 for encryption or 2 for decryption:\n").strip()
if option != "1" and option != "2":
    print("Invalid option.")
    quit()

phrase = raw_input("Enter phrase:\n").strip()
key = raw_input("Enter key:\n").strip()

if option == "1":
    print("Result:\n" + encrypt(phrase, key))
elif option == "2":
    print("Result:\n" + decrypt(phrase, key))