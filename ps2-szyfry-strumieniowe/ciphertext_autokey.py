from linear_feedback_shift_register import LFSR_RNG

# dziedziczymy z generatora liczb pseudolosowych LFSR
class CiphertextAutokeyEncryptor(LFSR_RNG):

    def __init__(self, coefficients, seed):
        LFSR_RNG.__init__(self, coefficients, seed)

    def encrypt_bit(self, value = False):

        # XOR na wartosciach rejestru dla ktorych wspolczynniki nie sa rowne 0 i potem XOR na dostarczonej wartosci
        new_val = value != LFSR_RNG._calculate_new_val(self)
        result = 1 if new_val else 0

        # obcinamy ostatnia wartosc rejestru i dodajemy nowa na poczatku
        self.register = [new_val] + self.register[:self.level - 1]
        return result

class CiphertextAutokeyDecryptor(LFSR_RNG):

    def __init__(self, coefficients, seed):
        LFSR_RNG.__init__(self, coefficients, seed)
    
    # odszyfruj bit
    def decrypt_bit(self, value):

        result = 1 if (value != LFSR_RNG._calculate_new_val(self)) else 0
        # obcinamy ostatnia wartosc rejestru i dodajemy nowa na poczatku
        self.register = [value] + self.register[:self.level - 1]
        return result

#-------------------------------------------------------------------------------------------------
coefficients = [1, 0, 0, 1]
seed = [0, 0, 1, 1]
print("Uzyte ziarno: %s || Uzyty wielomian: %s" % (str(seed),str(coefficients)))
# wejscie 
bits = [1, 1, 1, 0, 1, 0, 0, 1]
print("Przed zaszyfrowaniem: %s" % str(bits))

# szyfrowanie
encryptor = CiphertextAutokeyEncryptor(coefficients, seed)
encrypted = []
for v in bits:
    encrypted.append(encryptor.encrypt_bit(v))
print("Po zaszyfrowaniu: %s" % str(encrypted))

# deszyfrowanie
decrypted = []
decryptor = CiphertextAutokeyDecryptor(coefficients, seed)
for v in [0, 0, 1, 1, 0, 0, 1, 1]:
    decrypted.append(decryptor.decrypt_bit(v))
print("Po odszyfrowaniu: %s" % str(decrypted))