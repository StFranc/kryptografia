from linear_feedback_shift_register import LFSR_RNG

class SynchronousStreamCipher(LFSR_RNG):
    def __init__(self, coefficients, seed):
        LFSR_RNG.__init__(self, coefficients, seed)
    
    def encrypt_bit(self, bit):
        new_val = LFSR_RNG.next_binary(self)
        return bit != new_val
#--------------------------------------------------------------------------------------------------
coefficients = [1,0,0,1]
seed = [0, 0, 1, 0]
print("Uzyte ziarno: %s || Uzyty wielomian: %s" % (str(seed),str(coefficients)))

# wejscie
bits = [1,0,1,0,1,1,1,1,1,1,0,0,0,0,1]
print("Przed zaszyfrowaniem: %s" % str(bits))
        
# szyfrowanie
encryptor = SynchronousStreamCipher(coefficients, seed)
encrypted = []
for b in bits:
    encrypted.append(int(encryptor.encrypt_bit(b)))
print("Po zaszyfrowaniu: %s" % str(encrypted))

# deszyfrowanie
decryptor = SynchronousStreamCipher(coefficients, seed)
decrypted =[]
for b in encrypted:
    decrypted.append(int(encryptor.encrypt_bit(b)))
print("Po odszyfrowaniu: %s" % str(decrypted))