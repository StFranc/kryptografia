#from functools import reduce

class LFSR_RNG:
    # konstruktor
    def __init__(self, coefficients, initial_register = None):
        # jesli przekazany int - tworzymy domyslna tablice wspolczynnikow wielomianu o wartosciach binarnych [...0, 0, 0, 1, 1]
        if isinstance(coefficients, (int, long)):
            coefficients = [True if x == 0 or x == coefficients - 1 else False for x in range(coefficients)]
        
        self.level = len(coefficients)
        
        # jesli nie otrzymano poczatkowego rejestru to tworzymy rejestr o wartosciach binarnych [1, 0, 0, 0...]
        if initial_register == None:
            initial_register = [True if x == 0 else False for x in range(self.level)]
        elif self.level != len(initial_register):
            raise ValueError("Invalid initial_register length.")

        self.coefficients = coefficients
        self.register = initial_register
        
    # wylicza nastepna wartosc dodawana do rejestru na podstawie tablicy wspolczynnikow i rejestru oraz operacji XOR
    def _calculate_new_val(self):
        result = False
        for (i, x) in enumerate(self.register):
            result = result != (x and self.coefficients[i])
        return result

    # metoda pobierajaca jeden bit z rejestru
    def next_binary(self):
        # pobieramy ostatni element od prawej ktory jest naszym wynikiem
        result = 1 if self.register[self.level-1] else 0

        # XOR na wartosciach rejestru dla ktorych wspolczynniki nie sa rowne 0
        new_val = self._calculate_new_val()

        # obcinamy ostatnia wartosc rejestru i dodajemy nowa na poczatku
        self.register = [new_val] + self.register[:self.level - 1]
        # Wydruk rejestru
        return int(result)

    # metoda pobierajaca dowolna liczbe bitow i konwertujaca je na liczbe dziesietna
    def next_decimal(self, bits):
        # stworz tablice kolejnych bitow wyciaganych z rejestru
        values = [self.next_binary() for x in range(bits)]
        
        # wylicz z otrzymanych wartosci liczbe dziesietna
        result = sum(map(lambda (i, x): x * pow(2, i), enumerate(values)))
        return result
    
    def get_register(self):
        return map(lambda x: int(x), self.register)

#----------------------------------------------------------------------------------


if __name__ == "__main__":
    coefficients = [1,0,0,1]
    seed = [1,0,0,0]
    print("Uzyte ziarno: %s || Uzyty wielomian: %s" % (str(seed),str(coefficients)))
    rng = LFSR_RNG(coefficients,seed)
    for i in range(20):
        print("%s: %s" % (str(i + 1), str(rng.get_register())))
        rng.next_binary()
    