import itertools

class DESFunction:

    _s1=[
    [14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7],
    [0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8],
    [4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0],
    [15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13]]

    _s2=[
    [15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10],
    [3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5],
    [0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15],
    [13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9]]

    _s3=[
    [10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8],
    [13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1],
    [13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7],
    [1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12]]

    _s4=[
    [7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15],
    [13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9],
    [10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4],
    [3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14]]

    _s5=[
    [2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9],
    [14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6],
    [4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14],
    [11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3]]

    _s6=[
    [12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11],
    [10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8],
    [9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6],
    [4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13]]

    _s7=[
    [4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1],
    [13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6],
    [1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2],
    [6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12]]

    _s8=[
    [13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7],
    [1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2],
    [7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8],
    [2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11]]

    _s_boxes = [_s1, _s2, _s3, _s4, _s5, _s6, _s7, _s8]

    # tablica rozszerzajaca polowe bloku z 32 bit do 48 bit
    _expansion_array = [
        32,  1,  2,  3,  4,  5,
         4,  5,  6,  7,  8,  9,
         8,  9, 10, 11, 12, 13,
        12, 13, 14, 15, 16, 17,
        16, 17, 18, 19, 20, 21,
        20, 21, 22, 23, 24, 25,
        24, 25, 26, 27, 28, 29,
        28, 29, 30, 31, 32,  1]
    
    _permutation_p_array = [
        16, 7, 20, 21,
        29, 12, 28, 17,
        1, 15, 23, 26,
        5, 18, 31, 10,
        2, 8, 24, 14,
        32, 27, 3, 9,
        19, 13, 30, 6,
        22, 11, 4, 25]

    # rozszerza 32-bitowa czesc r bloku do 48 bitow
    def _expand_r(self, part):
        result = []
        for v in self._expansion_array:
            result.append(part[v-1])
        return result
    
    # XORowanie 48-bitowej tablicy R i 48 bitowego klucza w celu uzyskania nowej tablicy
    def _xor_key_and_r(self, r, key):
        result = []
        for i in range(48):
            # XOR na r[i] oraz key[i]
            result.append(r[i]^key[i])
        return result

    # dzieli 48-bitowy ciag na 8 ciagow 6-bitowych
    def _divide_in_8(self, bits):
        result = []
        for i in range(8):
            result.append(bits[i*6:(i+1)*6])
        return result
    
    # wyciaga z 6-bitowego ciagu numer wiersza (pierwszy i ostatni bit) i kolumny(srodkowe bity)
    def _get_line_and_column(self, bits):
        line = bits[0]*2 + bits[5]
        column = bits[1]*2*2*2 + bits[2]*2*2 + bits[3]*2 + bits[4]
        return (line, column)
    
    # konwertuje int do 4-bitowej tablicy wartosci zerojedynkowych
    def _convert_int_to_binary(self, number):
        result = [0 for i in range(4)]
        iterator = 3
        while number != 0:
            bit = number % 2
            result[iterator] = bit
            iterator -= 1
            number /= 2
        return result
    
    # koncowa permutacja 32-bitowego ciagu 
    def _permutate_with_p(self, bits):
        result = []
        for v in self._permutation_p_array:
            result.append(bits[v-1])
        return result
    
    # wykonaj funkcje szyfrujaca na 32-bitowym ciagu przy pomocy 48-bitowego klucza
    def perform_function(self, bits, key):

        # rozszerzenie ciagu z 32 bit do 48 bit
        bits = self._expand_r(bits)
    
        # zXORowanie pozyskanego 48-bitowego ciagu z 48-bitowym kluczem
        bits = self._xor_key_and_r(bits, key)
    
        # podzial uzyskanego ciagu 48-bitowego na 8 ciagow 6-bitowych
        bits_8x6 = self._divide_in_8(bits)

        # przepuszczenie kazdego z 8 ciagow przez jedno z 8 pudelek s i otrzymanie 8 ciagow 4-bitowych
        new_bits = []
        for (i, b) in enumerate(bits_8x6):

            # pobieramy numer lini i kolumny pudelka s z kadego 6-bitowego ciagu
            (line, column) = self._get_line_and_column(b)

            # pobieramy odopwiednia wartosc z odpowiedniego pudelka s
            value = self._s_boxes[i][line][column]

            # konwertujemy uzyskana wartosc do 4-bitowej reprezentacji binarnej
            b = self._convert_int_to_binary(value)
            new_bits.append(b)
        
        # laczymy uzyskane ciagi 4-bitowe w jeden 32-bitowy
        bits = list(itertools.chain.from_iterable(new_bits))

        # permutujemy uzyskany ciag 32-bitowy przy pomocy tablicy p
        bits = self._permutate_with_p(bits)

        return bits
