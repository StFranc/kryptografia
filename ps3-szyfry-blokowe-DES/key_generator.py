

from collections import deque

class DESKeyGenerator:

    # tablica wstepnej permutacji
    _permuted_choice1 = [
        57, 49, 41, 33, 25, 17,  9,
         1, 58, 50, 42, 34, 26, 18,
        10,  2, 59, 51, 43, 35, 27,
        19, 11,  3, 60, 52, 44, 36,
        63, 55, 47, 39, 31, 23, 15,
         7, 62, 54, 46, 38, 30, 22,
        14,  6, 61, 53, 45, 37, 29,
        21, 13,  5, 28, 20, 12,  4]
    
    # tablica permutacji dla kolejnych kluczy
    _permuted_choice2 =[
        14, 17, 11, 24,  1,  5,
         3, 28, 15,  6, 21, 10,
        23, 19, 12,  4, 26,  8,
        16,  7, 27, 20, 13,  2,
        41, 52, 31, 37, 47, 55,
        30, 40, 51, 45, 33, 48,
        44, 49, 39, 56, 34, 53,
        46, 42, 50, 36, 29, 32]

    # permutujemy 64-bitowy klucz do 56-bit na podstawie tablicy _permuted_choice1
    def _permute_key1(self, key):
        result = []
        for v in self._permuted_choice1:
            result.append(key[v-1])
        return result
    
    # permutujemy 56-bitowy klucz do 48-bit na podstawie tablicy _permuted_choice2
    def _permute_key2(self, key):
        result = []
        for v in self._permuted_choice2:
            result.append(key[v-1])
        return result

    # dzielimy klucz na kolejki C i D po 28 bit kazda
    def _divide_key(self, key):
        c = deque(key[:28])
        d = deque(key[28:])
        return (c,d)

    # tabela z iloscia bitow do przesuniecia w celu uzyskania klucza dla kolejnych kolejek
    _shifts = [1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1]

    # tworzy klucze dla wszystkich 16 kolejek
    def generate_keys(self, key):
        # permutujemy i zmniejszamy klucz z 64 bit do 56 bit
        permuted_key = self._permute_key1(key)

        # dzielimy klucz na 2 czesci
        (c, d) = self._divide_key(permuted_key)

        # tworzymy klucze dla kolejnych rund
        result = []
        for v in self._shifts:

            # obydwie czesci przesuwamy w lewo na podstawie tablicy _shifts
            c.rotate(-v)
            d.rotate(-v)

            # c i d laczymy a nastepnie permutujemy przy pomocy tablicy _permuted_choice2 zmniejszajac z 56 bit do 48 bit
            new_key = self._permute_key2(list(c) + list(d))
            result.append(new_key)

        return result
