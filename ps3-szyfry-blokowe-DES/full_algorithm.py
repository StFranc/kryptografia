# DES - szyfr blokowy
# szyfrowanie: 8 bajtow na blok = 64 bity
# klucz: 56 bitow, wkladamy 64 i ignorujemy indexy dla ktorych i mod 8 = 0
# strona z przykladowymi wartosciami do porownania: http://page.math.tu-berlin.de/~kant/teaching/hess/krypto-ws2006/des.htm

from key_generator import DESKeyGenerator
from function import DESFunction

from collections import deque

class DESAlgorithm:

    # konstruktor tworzacy klucze i funkcje szyfrujaca
    def __init__(self, key):
        # stworz obiekt generator kluczy
        self._key_gen = DESKeyGenerator()

        # stworz obiekt z funkcja szyfrujaca
        self._function = DESFunction()

        # stworz zestaw kluczy szyfrujacych
        self._encryption_keys = self._key_gen.generate_keys(key)

        # stworz zestaw kluczy deszyfrujacych
        self._decryption_keys = self._encryption_keys[::-1]

    # tablica poczatkowej permutacji
    _initial_permutation = [
        58, 50, 42, 34, 26, 18, 10,  2,
        60, 52, 44, 36, 28, 20, 12,  4,
        62, 54, 46, 38, 30, 22, 14,  6,
        64, 56, 48, 40, 32, 24, 16,  8,
        57, 49, 41, 33, 25, 17,  9,  1,
        59, 51, 43, 35, 27, 19, 11,  3,
        61, 53, 45, 37, 29, 21, 13,  5,
        63, 55, 47, 39, 31, 23, 15,  7]
    
    # tablica koncowej permutacji
    _inverse_initial_permutation = [
        40, 8, 48, 16, 56, 24, 64, 32,
        39, 7, 47, 15, 55, 23, 63, 31,
        38, 6, 46, 14, 54, 22, 62, 30,
        37, 5, 45, 13, 53, 21, 61, 29,
        36, 4, 44, 12, 52, 20, 60, 28,
        35, 3, 43, 11, 51, 19, 59, 27,
        34, 2, 42, 10, 50, 18, 58, 26,
        33, 1, 41, 9, 49, 17, 57, 25]

    # wtepnie permutuje 64-bitowy blok danych
    def _permutate_block(self, block):
        result = []
        for v in self._initial_permutation:
            result.append(block[v-1])
        return result

    # koncowo permutuje zaszyfrowany 64-bitowy blok danych
    def _permutate_block2(self, block):
        result = []
        for v in self._inverse_initial_permutation:
            result.append(block[v-1])
        return result

    
    # podzial spermutowanego 64-bitowego bloku na dwa bloki 32-bitowe
    def _divide_block(self, block):
        l = block[:32]
        r = block[32:]
        return (l, r)
    # XORowanie dwoch blokow 32-bitowych
    def _xor_l_with_r(self, l, r):
        result = []
        for i in range(32):
            result.append(l[i]^r[i])
        return result
    
    # zaszyfruj 64-bitowy blok
    def _process_block(self, block, keys):

        # permutuje 64-bitowy blok przy pomocy _initial_permutation
        block = self._permutate_block(block)

        # podzial spermutowanego bloku na 2 czesci
        (l, r) = self._divide_block(block)

        # wykonanie 16 rund szyfrowania
        for i in range(16):

            # przepuszczamy 32-bitowe r przez funkcje szyfrujaca podajac jeden z 48-bitowych kluczy
            new_r = self._function.perform_function(r, keys[i])

            # XORujemy uzyskane new_r z l i w ten sposob uzyskujemy r dla nastepnej iteracji
            new_r = self._xor_l_with_r(l, new_r)

            # l dla nastepnej iteracji jest rowne r z obecnej iteracji
            l = r
            r = new_r
        
        # scalamy blok lewy i prawy odwracajac kolejnosc
        block = r + l

        # przepuszczamy otrzymany blok przez _inverse_initial_permutation
        block = self._permutate_block2(block)

        return block

    # funkcja szyfrujaca
    def encrypt_block(self, block):
        return self._process_block(block, self._encryption_keys)
    
    # funkcja deszyfrujaca
    def decrypt_block(self, block):
        return self._process_block(block, self._decryption_keys)
    
    def get_keys(self):
        return self._encryption_keys

# przykladowy 64-bitowy klucz
key = [0,0,0,1,0,0,1,1,0,0,1,1,0,1,0,0,0,1,0,1,0,1,1,1,0,1,1,1,1,0,0,1,1,0,0,1,1,0,1,1,1,0,1,1,1,1,0,0,1,1,0,1,1,1,1,1,1,1,1,1,0,0,0,1]
# przykladowy 64-bitowy blok danych
data_block = [0,0,0,0,0,0,0,1,0,0,1,0,0,0,1,1,0,1,0,0,0,1,0,1,0,1,1,0,0,1,1,1,1,0,0,0,1,0,0,1,1,0,1,0,1,0,1,1,1,1,0,0,1,1,0,1,1,1,1,0,1,1,1,1]

#stowrzenie obiektu z algorytmem i wstrzykniecie zestawu kluczy + funkcji
des_algorithm = DESAlgorithm(key)

# zaszyfrowanie bloku danych
encrypted_block = des_algorithm.encrypt_block(data_block)

# zdeszyfrowanie bloku danych
decrypted_block = des_algorithm.decrypt_block(encrypted_block)



print("Klucz glowny:")
print(key)
print("Wygenerowane klucze:")
print(des_algorithm.get_keys())
print("Przed zaszyfrowaniem:")
print(data_block)
print("Po zaszyfrowaniu:")
print(encrypted_block)
print("Po zdeszyfrowaniu:")
print(decrypted_block)
